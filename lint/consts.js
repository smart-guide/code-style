module.exports = {
	// config codes
	ignore: 0,
	warn: 1,
	error: 2,

	// constants
	INDENT: "tab",
	MAX_SPECIFICITY: 20
};
