// eslint-disable-next-line @typescript-eslint/no-var-requires
const base = require("./base");
// eslint-disable-next-line @typescript-eslint/no-var-requires
const style = require("./style");

module.exports = {
	rules: Object.assign({}, base.rules, style.rules)
};

